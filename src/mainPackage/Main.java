package mainPackage;

import mainPackage.StudentGUI.StartFrame;
import mainPackage.commands.Repozitory;
import mainPackage.commands.Student;

import java.io.*;

/**
 * Created by Smolentsev Il'ya on 27.11.2019.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        createTestDataSet();
        new StartFrame(); //GUI
        //Menu.CreateMainMenu().execute(); //консольный вариант

    }

    private static void createTestDataSet() {
        for (int i = 0; i < 5; ++i) {
            Student stud = new Student();
            stud.studentNumber = i;
            stud.firstName = "FirstNameTest";
            stud.groopName = "GroopNameTest";
            stud.lastName = "LastNameTest" + i;
            stud.patronymic = "patronymicTest";
            stud.mark = 5;
            Repozitory.getInstance().addStudent(stud);
        }
        //Repozitory.getInstance().writingFileBytes();
    }
}



