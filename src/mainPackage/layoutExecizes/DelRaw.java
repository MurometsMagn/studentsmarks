package mainPackage.layoutExecizes;

import javax.swing.*;
import java.util.Arrays;

public class DelRaw extends JFrame {

    public DelRaw() {
        String data[][] = {{"1", "Stackoverflow"}, {"2", "Ru Stackoverflow"}, {"3", "Codereview"}};
        String column[] = {"ID", "NAME"};
        JTable jTable = new JTable(data, column);

        final JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem deleteItem = new JMenuItem("Delete");
        popupMenu.add(deleteItem);

        deleteItem.addActionListener(e -> System.out.println("Selected rows: " + Arrays.toString(jTable.getSelectedRows())));

        jTable.setComponentPopupMenu(popupMenu);

        JScrollPane scrollPane = new JScrollPane(jTable);
        add(scrollPane);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }

    public static void main(String[] args) {
        new DelRaw();
    }
}
