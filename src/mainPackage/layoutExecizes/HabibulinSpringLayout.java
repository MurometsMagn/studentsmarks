package mainPackage.layoutExecizes;

import javax.swing.*;

public class HabibulinSpringLayout extends JFrame {
    JComponent[] comp = {
            new JButton("Длинная надпись"),
            new JButton("<html>Надпись с<p> двумя строками"),
            new JButton("OK")
    };

    public HabibulinSpringLayout() {
        super(" Размещение SpringLayout");
        SpringLayout s1 = new SpringLayout();
        setLayout(s1);


        //Задаем величину промежутка между компонентами
        Spring xPad = Spring.constant(6);

        //Задаем величину отступа от границ контейнера
        Spring yPad = Spring.constant(10);

        //Текущее положение левого верхнего угла
        Spring currX = yPad;

        //Наибольшая высота компонента, пока 0
        Spring maxHeight = Spring.constant(0);

        for (int i = 0; i < comp.length; i++) {
            add(comp[i]);
            //Получаем размер i-го компонента
            SpringLayout.Constraints cons = s1.getConstraints(comp[i]);
            //Устанавливаем положение i-го компонента
            cons.setX(currX);
            cons.setY(yPad);
            //Перемещаем текущее положение угла
            currX = Spring.sum(xPad, cons.getConstraint("East"));
            //Изменяем наибольшую высоту
            maxHeight = Spring.max(maxHeight, cons.getConstraint("South"));
        }

        //получаем размеры контейнера
        SpringLayout.Constraints pCons = s1.getConstraints(getContentPane());
        //устанавливаем размеры всего содержимого контейнера
        pCons.setConstraint(SpringLayout.EAST, Spring.sum(currX, yPad)); //ширина
        pCons.setConstraint(SpringLayout.SOUTH, Spring.sum(maxHeight, yPad)); //высота

        setLocationRelativeTo(null);
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new HabibulinSpringLayout();
    }
}
