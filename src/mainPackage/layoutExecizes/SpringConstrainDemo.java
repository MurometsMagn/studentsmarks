package mainPackage.layoutExecizes;

import javax.swing.*;
import java.awt.*;

/**
 * https://www.youtube.com/watch?v=uOZMtVjcKcs&list=PLa4NwJuV2AfsPvUkW42w1DgU1_4Fxtftj&index=20
 */
public class SpringConstrainDemo {
    JFrame frame = new JFrame("Spring Constraints Examples");
    JButton button1 = new JButton("Button 1");
    JButton button2 = new JButton("Button 2");
    JButton button3 = new JButton("Button 3");
    JButton button4 = new JButton("Button 4");
    JButton button5 = new JButton("Button 5");
    SpringLayout springLayout = new SpringLayout();

    SpringLayout.Constraints springConstraints1 = new SpringLayout.Constraints();
    SpringLayout.Constraints springConstraints2 = new SpringLayout.Constraints();
    SpringLayout.Constraints springConstraints3 = new SpringLayout.Constraints();
    SpringLayout.Constraints springConstraints4 = new SpringLayout.Constraints();
    SpringLayout.Constraints springConstraints5 = new SpringLayout.Constraints();

    Container container = frame.getContentPane();

    SpringConstrainDemo() {
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        container.setLayout(springLayout);

        Spring xPadding1 = Spring.constant(20);
        Spring yPadding1 = Spring.constant(20);
        springConstraints1.setX(xPadding1);
        springConstraints1.setY(yPadding1);
        springConstraints1.setHeight(Spring.constant(60));

        container.add(button1, springConstraints1);

        Spring xPadding2 = Spring.constant(40);
        Spring yPadding2 = Spring.constant(40);
        springConstraints2.setX(xPadding2);
        springConstraints2.setY(yPadding2);
        container.add(button2, springConstraints2);

        Spring xPadding3 = Spring.constant(60);
        Spring yPadding3 = Spring.constant(60);
        springConstraints3.setX(xPadding3);
        springConstraints3.setY(yPadding3);
        container.add(button3, springConstraints3);

        Spring xPadding4 = Spring.constant(80);
        Spring yPadding4 = Spring.constant(80);
        springConstraints4.setX(xPadding4);
        springConstraints4.setY(yPadding4);
        container.add(button4, springConstraints4);

        Spring xPadding5 = Spring.constant(110);
        Spring yPadding5 = Spring.constant(110);
        springConstraints5.setX(xPadding5);
        springConstraints5.setY(yPadding5);
        container.add(button5, springConstraints5);
    }

    public static void main(String[] args) {
        new SpringConstrainDemo();
    }
}
