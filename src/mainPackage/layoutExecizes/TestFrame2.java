package mainPackage.layoutExecizes;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class TestFrame2 extends JFrame {

    static int i = 0;

    public TestFrame2() {

        super("Тестовое окно");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ArrayList<MyBean> beans = new ArrayList<MyBean>();

        for (int i = 0; i < 30; i++) {
            beans.add(new MyBean("Имя " + i, "Размер " + i, "Описание " + i));
        }

        TableModel model = new MyTableModel(beans);
        JTable table = new JTable(model);

        getContentPane().add(new JScrollPane(table));

        setPreferredSize(new Dimension(260, 220));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                new TestFrame2();
            }
        });
    }

    public class MyTableModel implements TableModel {

        private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

        private List<MyBean> beans;

        public MyTableModel(List<MyBean> beans) {
            this.beans = beans;
        }

        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }

        //Сущности могут быть разные. Поля у них естественно могут быть разными,
        //однако, JTable должен знать, какие данные он должен отобразить в какой колонке.
        //В MyBean все поля у нас строковые, поэтому в нашем случае метод таков
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        //количество столбцов которое будет отображаться в таблице
        public int getColumnCount() {
            return 3;
        }

        //заголовок колонки по её индексу
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "Имя";
                case 1:
                    return "Размер";
                case 2:
                    return "Описание";
            }
            return "";
        }

        //количество строк, которое будет отображаться в таблице
        public int getRowCount() {
            return beans.size();
        }

        //данные в каких ячейках JTable будут показываться
        public Object getValueAt(int rowIndex, int columnIndex) {
            MyBean bean = beans.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return bean.getName();
                case 1:
                    return bean.getSize();
                case 2:
                    return bean.getDescription();
            }
            return "";
        }

        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }

        // public boolean isCellEditable(int rowIndex, int columnIndex) {return false;}
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return true;
        }

        //  public void setValueAt(Object value, int rowIndex, int columnIndex) {}
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            MyBean b = beans.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    b.setName((String) aValue);
                    return;
                case 1:
                    b.setSize((String) aValue);
                case 2:
                    b.setDescription((String) aValue);
            }
        }
    }

    //список сущностей MyBean
    public class MyBean {

        private String name;
        private String size;
        private String description;

        public MyBean(String name, String size, String description) {
            this.setName(name);
            this.setSize(size);
            this.setDescription(description);
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSize() {
            return size;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }
}
