package mainPackage.commands;

public interface Visitor {
    void handle(Student student);
}
