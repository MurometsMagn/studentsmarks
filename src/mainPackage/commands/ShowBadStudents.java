package mainPackage.commands;

public class ShowBadStudents extends Command {

    public ShowBadStudents() {
        super("Показать неуспевающих");
    }

    @Override
    public void execute() {
//        Iterator i = Repozitory.getInstance().getIterator();
//        while (i.hasNext()) {
//            Student student = i.next();
//            if (student.mark <= 2) {
//                Utilites.printStudent(student);
//            }
//        }

        Visitor visitor = new Visitor() {
            @Override
            public void handle(Student student) {
                if (student.mark <= 2) {
                    Utilites.printStudent(student);
                }
            }
        };

        Repozitory.getInstance().handleOfStudent(visitor);


    }
}
