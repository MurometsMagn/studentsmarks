package mainPackage.commands;

public interface ICommand {
    void execute();

    String getTitle();
}
