package mainPackage.commands;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Smolentsev Il'ya on 04.12.2019.
 */

public class Repozitory { //Singleton
    public static final int strLength = 25;
    private static File f = new File("bytesInOut.txt");
    private List<Student> studentList = new ArrayList<>();

    //Singleton
    private static Repozitory instance;

    public static Repozitory getInstance() {
        if (instance == null) {
            instance = new Repozitory();
        }
        return instance;
    }

    private Repozitory() { //приватный конструктор
        readingFileBytes();

        for (int i = 0; i < studentList.size(); i++) {
            studentList.get(i).studentNumber = i;
        }
    }

    public Iterator getIterator() {
        return new Iterator(studentList);
    }

    public void handleOfStudent(Visitor visitor) {
        for (int i = 0; i < studentList.size(); i++) {
            Student student = studentList.get(i);
            visitor.handle(student);
        }
    }

    public void readingFileBytes() {
        try (InputStream inputStream = new FileInputStream(f)) {
            byte[] str = new byte[strLength]; //в массиве строка
            outerLoop:
            for (; ; ) {
                String fragment[] = new String[6];
                for (int i = 0; i <= 5; i++) {
                    if (inputStream.read(str) == -1) {
                        break outerLoop;
                    } else {
                        fragment[i] = new String(str).trim();
                    }
                }
                Student student = new Student(Integer.parseInt(fragment[0]), fragment[1], fragment[2], fragment[3],
                        fragment[4], Integer.parseInt(fragment[5]));
                studentList.add(student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeStringBytes(OutputStream outputStream, String str) throws IOException {
        ByteBuffer b = ByteBuffer.allocate(strLength);
        b.put(str.getBytes());
        outputStream.write(b.array());
    }

    public void writingFileBytes() {
        try (OutputStream outputStream = new FileOutputStream(f)) {
            for (Student student : studentList) {
                writeStringBytes(outputStream, String.valueOf(student.studentNumber));
                writeStringBytes(outputStream, student.lastName);
                writeStringBytes(outputStream, student.firstName);
                writeStringBytes(outputStream, student.patronymic);
                writeStringBytes(outputStream, student.groopName);
                writeStringBytes(outputStream, String.valueOf(student.mark));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Student getStudent(int index) {
        return studentList.get(index);
    }

    public void addStudent(Student student) {
        if (student == null) {
            throw new NullPointerException();
        }
        studentList.add(student);
        //writingFileBytes();
    }

    public void deleteStudent(int index) {
        studentList.remove(index);
        //writingFileBytes();
    }

    public int countStudent() {
        return studentList.size();
    }
}
