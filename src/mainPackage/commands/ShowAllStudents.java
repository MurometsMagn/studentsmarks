package mainPackage.commands;

class ShowAllStudents extends Command {

    ShowAllStudents() {
        super("Показать всех студентов");
    }

    @Override
    public void execute() {
        Utilites.printHeader();
        Visitor visitor = new Visitor() {
            @Override
            public void handle(Student student) {
                Utilites.printStudent(student);
            }
        };
        Repozitory.getInstance().handleOfStudent(visitor);


        /* шаблон Итератор
        @Override
        void execute () {
            Utilites.printHeader();
            for (int i = 0; i < Repozitory.getInstance().countStudent(); i++) {
                final Student student = Repozitory.getInstance().getStudent(i);
                Utilites.printStudent(student);
            }

        // исправить так:
        Iterator it = Repozitory.getInstance().getIterator();
        while (it.hasNext()) {
            Student student = it.next();
        }*/
    }
}
