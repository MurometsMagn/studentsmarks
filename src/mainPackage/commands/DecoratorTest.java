package mainPackage.commands;

public class DecoratorTest {
    public static void main(String[] argv) {
        IObject objA = new PrintDecorator(new ObjectA());
        IObject objB = new PrintDecorator(new PrintDecorator2(new ObjectB()));
        objA.execute();
        objB.execute();
    }
}

interface IObject {
    void execute();
}

class ObjectA implements IObject {
    @Override
    public void execute() {
        System.out.println("TestA");
    }
}

class ObjectB implements IObject {
    @Override
    public void execute() {
        System.out.println("TestB");
    }
}

class PrintDecorator implements IObject {
    private IObject obj;

    public PrintDecorator(IObject obj) {
        this.obj = obj;
    }

    @Override
    public void execute() {
        System.out.println("{");
        obj.execute();
        System.out.println("}");
    }


}

class PrintDecorator2 implements IObject {
    private IObject obj;

    public PrintDecorator2(IObject obj) {
        this.obj = obj;
    }

    @Override
    public void execute() {
        System.out.println("[");
        obj.execute();
        System.out.println("]");
    }
}


