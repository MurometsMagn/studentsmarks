package mainPackage.commands;

import java.util.InputMismatchException;

public class AddStudent extends Command {

    public AddStudent() {
        super("Добавить студента");
    }

    @Override
    public void execute() {
        Student student = new Student();
        student.studentNumber = Repozitory.getInstance().countStudent() + 1;

        Utilites.scanner.nextLine(); //перевод строки?
        do { //нужны ли регулярки?
            System.out.println("Введите фамилию:");
            student.lastName = Utilites.scanner.nextLine();
        } while (student.lastName.trim().equals(""));

        do {
            System.out.println("Введите имя:");
            student.firstName = Utilites.scanner.nextLine();
        } while (student.firstName.trim().isEmpty());

        do {
            System.out.println("Введите отчество:");
            student.patronymic = Utilites.scanner.nextLine();
        } while (student.patronymic.trim().equals(""));

        do {
            System.out.println("Введите название группы:");
            student.groopName = Utilites.scanner.nextLine();
        } while (student.groopName.trim().equals(""));

        System.out.println("Введите оценку студента:");
        boolean continueInput = true;
        int inputNumber = 0;
        do {
            try {
                inputNumber = Utilites.scanner.nextInt();
                if (inputNumber >= 1 && inputNumber <= 5) {
                    student.mark = inputNumber;
                    continueInput = false;
                } else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                Utilites.scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        } while (continueInput);

        Repozitory.getInstance().addStudent(student);
    }
}
