package mainPackage.commands;

import mainPackage.commands.decorators.ExecuteDecorator;
import mainPackage.commands.decorators.FirstDecorator;
import mainPackage.commands.decorators.SecondDecorator;
import mainPackage.commands.decorators.ThirdDecorator;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class Menu extends Command {
    private List<ICommand> commandsList = new ArrayList<>();

    public Menu(String title) {
        super(title);
    }

    @Override
    public void execute() {

        // label:
        for (; ; ) {
            System.out.println(title);
            for (int i = 0; i < commandsList.size(); i++) {
                System.out.println((i + 1) + " - " + commandsList.get(i).getTitle());
            }
            System.out.println(commandsList.size() + 1 + " -  отмена");

            boolean continueInput = true;
            int inputNumber = 0;
            do {
                try {
                    inputNumber = Utilites.scanner.nextInt();
                    Utilites.scanner.nextLine();
                    if (inputNumber == (commandsList.size() + 1)) {
                        //return into the mainMenu
                        return;
                        // continue label;
                    } else if (inputNumber > 0 && inputNumber <= commandsList.size()) {
                        continueInput = false;
                    } else System.out.println("некоректное значение, попробуйте еще раз");
                } catch (InputMismatchException ex) {
                    System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                    Utilites.scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
                }
            } while (continueInput);

            commandsList.get(inputNumber - 1).execute();
        }
    }

    public static Menu CreateMainMenu() {
        Menu mainMenu = new Menu("Главное меню");

        mainMenu.commandsList.add(new FirstDecorator(new ShowAllStudents()));
        mainMenu.commandsList.add(new SecondDecorator(new ShowAllExellentStudents()));
        mainMenu.commandsList.add(new ThirdDecorator(new ShowBadStudents()));
        mainMenu.commandsList.add(new ExecuteDecorator(new AddStudent()));
        mainMenu.commandsList.add(new ExecuteDecorator(new ChangeStudent()));
        mainMenu.commandsList.add(new ExecuteDecorator(new DeleteStudent()));

        return mainMenu;
    }

    public static Menu CreateSubMenu(Student student) {
        Menu submenu = new Menu("Изменить студента");

        //синтаксический     сахар
        //вложенные анонимные классы
        submenu.commandsList.add(new Command("Изменить имя") {
            @Override
            public void execute() {
                System.out.println(title);
                student.firstName = Utilites.scanner.nextLine();
            }
        });

        submenu.commandsList.add(new Command("Изменить фамилию") {
            @Override
            public void execute() {
                student.lastName = Utilites.scanner.nextLine();
            }
        });

        submenu.commandsList.add(new Command("Изменить отчество") {
            @Override
            public void execute() {
                student.patronymic = Utilites.scanner.nextLine();
            }
        });

        submenu.commandsList.add(new Command("Изменить название группы") {
            @Override
            public void execute() {
                student.groopName = Utilites.scanner.nextLine();
            }
        });

        submenu.commandsList.add(new Command("Изменить оценку") {
            @Override
            public void execute() {
                boolean continueInput = true;
                int inputNumber = 0;
                do {
                    try {
                        inputNumber = Utilites.scanner.nextInt();
                        if (inputNumber >= 1 && inputNumber <= 5) {
                            student.mark = inputNumber;
                            continueInput = false;
                        } else
                            System.out.println("некоректное значение, попробуйте еще раз");
                    } catch (InputMismatchException ex) {
                        System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                        Utilites.scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
                    }
                } while (continueInput);
            }
        });
        return submenu;
    }
}