package mainPackage.commands;

import java.util.List;

public class Iterator {
    private int index = 0;
    private List<Student> studentList;


    public Iterator(List<Student> studentList) {
        this.studentList = studentList;
    }

    Student next() {
        return studentList.get(index++);
    }

    boolean hasNext() { //есть ст-т или нет
        return index < studentList.size();
    }
}

