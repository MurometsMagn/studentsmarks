package mainPackage.commands;

public class ChangeStudent extends Command {

    public ChangeStudent() {
        super("Изменить студента");
    }

    @Override
    public void execute() {
        System.out.println("Введите номер студента, которого Вы хотите изменить:");
        new ShowAllStudents().execute();

        final Student student = Utilites.lookingForStudent(Repozitory.getInstance());

         /*submenu.menuCommandsList.add(new ChangeStudentName(student));
        второй способ (первый - синтетический сахар*/

        Menu submenu = Menu.CreateSubMenu(student);
        submenu.execute();

        //Repozitory.getInstance().writingFileBytes();


        /*
        Student student = new Student();
        System.out.println("Введите номер студента, которого Вы хотите изменить:");
        Utilites.printHeader();
        new ShowAllStudents().execute(repozitory);
        student = Utilites.lookingForStudent(repozitory);

        System.out.println("Введите номер поля, которое Вы хотите изменить:");
        System.out.println("2 — фамилия: " + student.lastName);
        System.out.println("3 — имя: " + student.firstName);
        System.out.println("4 — отчество: " + student.patronymic);
        System.out.println("5 — название группы: " + student.groopName);
        System.out.println("6 — оценка: " + student.mark);

        boolean continueInput = true;
        int inputNumber = 0;
        do {
            try {
                inputNumber = Utilites.scanner.nextInt();
                if (inputNumber >= 2 && inputNumber <= 6) {
                    continueInput = false;
                } else System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                Utilites.scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        } while (continueInput);

        System.out.println("Введите новое значение поля: ");
        if (inputNumber >= 2 && inputNumber <= 5) {
            Utilites.scanner.nextLine(); //перевод строки?
            String txt = Utilites.scanner.nextLine();
            switch (inputNumber) {
                case 2:
                    student.lastName = txt;
                    break;
                case 3:
                    student.firstName = txt;
                    break;
                case 4:
                    student.patronymic = txt;
                    break;
                case 5:
                    student.groopName = txt;
                    break;
            }
        } else if (inputNumber == 6) {
            continueInput = true;
            inputNumber = 0;
            do {
                try {
                    inputNumber = Utilites.scanner.nextInt();
                    if (inputNumber >= 1 && inputNumber <= 5) {
                        student.mark = inputNumber;
                        continueInput = false;
                    } else
                        System.out.println("некоректное значение, попробуйте еще раз");
                } catch (InputMismatchException ex) {
                    System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                    Utilites.scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
                }
            } while (continueInput);
        }

        System.out.println("полученные поля студента: ");
        Utilites.printStudent(student);
        */
    }
}
