package mainPackage.commands;

public class Student {
    public int studentNumber;
    public String lastName;
    public String firstName;
    public String patronymic;
    public String groopName;
    public int mark;

    public Student() {
    }

    public Student(int studentNumber, String lastName, String firstName, String patronymic, String groopName, int mark) {
        this.studentNumber = studentNumber;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.groopName = groopName;
        this.mark = mark;
    }
}
