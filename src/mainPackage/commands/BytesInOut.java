package mainPackage.commands;

/**
 *  побайтовый ввод-вывод из файла
 */

import java.io.*;
import java.nio.ByteBuffer;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BytesInOut<continueInput> {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число N:");
        boolean continueInput = true;
        int n = 0;
        do {
            try {
                n = scanner.nextInt();
                continueInput = false;
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        } while (continueInput);

        //writing
        File f = new File("bytesInOut.txt");
        try (OutputStream outputStream = new FileOutputStream(f)) {
            String endChar = "TEST";
            outputStream.write(ByteBuffer.allocate(4).putInt(endChar.length()).array());
            outputStream.write(endChar.getBytes());

            ByteBuffer buf = ByteBuffer.allocate(4 * n);
            for (int i = 1; i <= n; i++) {
                buf.putInt(i);
            }
            outputStream.write(buf.array());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //reading
        try (InputStream inputStream = new FileInputStream(f)) {
            byte[] data = new byte[4]; //в массиве число
            inputStream.read(data);
            ByteBuffer buf1 = ByteBuffer.wrap(data);
            int strSize = buf1.getInt();

            byte[] headr = new byte[strSize]; //в массиве строка
            inputStream.read(headr);
            String currentHeader = new String(headr);

            if (!currentHeader.equals("TEST")) {
                System.out.println("Error!");
                return;
            }

            for (int i = 1; ; i++) {
                byte[] b = new byte[4]; //в массиве число
                if (inputStream.read(b) == -1) break;

                ByteBuffer buf = ByteBuffer.wrap(b);
                int j = buf.getInt();
                System.out.println(i + ": " + j);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


