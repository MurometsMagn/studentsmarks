package mainPackage.commands.decorators;

import mainPackage.commands.*;

public class ThirdDecorator implements ICommand {
    int count =0;
    Command command;

    public ThirdDecorator(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
    }

    @Override
    public String getTitle() {
        count = 0;
        Visitor visitor = new Visitor() {
            @Override
            public void handle(Student student) {
                if (student.mark <= 2) {
                    count++;
                }
            }
        };
        Repozitory.getInstance().handleOfStudent(visitor);
        return command.getTitle() + " (" + count + ")";
    }
}
