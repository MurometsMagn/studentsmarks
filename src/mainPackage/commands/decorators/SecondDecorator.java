package mainPackage.commands.decorators;

import mainPackage.commands.*;

public class SecondDecorator implements ICommand {
    int count =0;
    Command command;

    public SecondDecorator(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
    }

    @Override
    public String getTitle() {
        count = 0;
        Visitor visitor = student -> {
            if (student.mark == 5) {
                count++;
            }
        };
        Repozitory.getInstance().handleOfStudent(visitor);
        return command.getTitle() + " (" + count + ")";
    }
}
