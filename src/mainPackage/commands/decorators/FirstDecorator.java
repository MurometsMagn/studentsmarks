package mainPackage.commands.decorators;

import mainPackage.commands.ICommand;
import mainPackage.commands.Command;
import mainPackage.commands.Repozitory;

public class FirstDecorator implements ICommand {
    Command command;

    public FirstDecorator(Command command){
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
    }

    @Override
    public String getTitle() {
        int count = Repozitory.getInstance().countStudent();
        return command.getTitle() + " (" + count + ")";
    }
}
