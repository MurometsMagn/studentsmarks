package mainPackage.commands.decorators;

import mainPackage.commands.Command;
import mainPackage.commands.ICommand;
import mainPackage.commands.Repozitory;

public class ExecuteDecorator implements ICommand {
    Command command;

    public ExecuteDecorator(Command command) {
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();
        Repozitory.getInstance().writingFileBytes();
    }

    @Override
    public String getTitle() {
        return command.getTitle();
    }
}
