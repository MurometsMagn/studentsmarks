package mainPackage.commands;

import java.util.Scanner;

public abstract class Command implements ICommand{ //зачем вообще нужен этот класс?
    String title;

    public String getTitle() {
        return title;
    }

    public Command(String title) { //конструктор
        this.title = title;
    }

    public abstract void execute();
}
