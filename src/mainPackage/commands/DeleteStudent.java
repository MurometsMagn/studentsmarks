package mainPackage.commands;

public class DeleteStudent extends Command {

    public DeleteStudent() {
        super("Удалить студента");
    }

    @Override
    public void execute() {
        Student student = new Student();
        System.out.println("Введите номер удаляемого студента:");
        new ShowAllStudents().execute();
        student = Utilites.lookingForStudent(Repozitory.getInstance());

        Repozitory.getInstance().deleteStudent(student.studentNumber);
    }
}
