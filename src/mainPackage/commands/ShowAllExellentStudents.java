package mainPackage.commands;

class ShowAllExellentStudents extends Command {

    public ShowAllExellentStudents() {
        super("Показать всех отличников");
    }

    @Override
    public void execute() {
        Visitor visitor = student -> {
            if (student.mark == 5) {
                Utilites.printStudent(student);
            }
        };
        Repozitory.getInstance().handleOfStudent(visitor);
    }

    /*@Override
    void execute() {
        for (int i = 0; i < Repozitory.getInstance().countStudent(); i++) {
            final Student student = Repozitory.getInstance().getStudent(i);
            if (student.mark == 5) {
                Utilites.printStudent(student);
            }
        }
    }
    */
}