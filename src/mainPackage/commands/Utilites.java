package mainPackage.commands;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Utilites {
    static Scanner scanner = new Scanner(System.in);

    static String printStudent(Student student) {
        String format = formatting();
        String studentN = String.format(format, String.valueOf(student.studentNumber), student.lastName, student.firstName,
                student.patronymic, student.groopName, String.valueOf(student.mark));
        System.out.println(studentN);
        return studentN;
    }

    private static String formatting() {
        int tmpLng = Repozitory.strLength; //25
        String format = "%1$-" + tmpLng + "s %2$-" + tmpLng + "s %3$-" +
                tmpLng + "s %4$-" + tmpLng +
                "s %5$-" + tmpLng + "s %6$-" + tmpLng + "s";
        //String format = "%1$-15s %2$-15s %3$-15s %4$-15s %5$-15s %6$-15s";
        return format;
    }

    static String printHeader() {
        String format = formatting();
        String header = String.format(format, "Number", "LastName", "FirstName", "Patronimic", "Groop", "Mark");
        System.out.println(header);
        return header;
    }

    static Student lookingForStudent(Repozitory repozitory) {
        Student foundStudent = new Student();
        boolean continueInput = true;
        int inputNumber = 0;
        do {
            try {
                inputNumber = scanner.nextInt();

                Iterator it = Repozitory.getInstance().getIterator();
                while (it.hasNext()) {
                    Student student = it.next();
                    if (inputNumber == student.studentNumber) {
                        foundStudent = student;
                        continueInput = false;
                    }
                }
                if (continueInput) {
                    System.out.println("некоректное значение, попробуйте еще раз");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        } while (continueInput);
        return foundStudent;
    }
}
