package mainPackage.StudentGUI;

import mainPackage.commands.Repozitory;
import mainPackage.commands.Student;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.HashSet;
import java.util.Set;

public class MyTableModel implements TableModel {
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
    private String[] columnName = {"№", "Фамилия", "Имя", "Отчество", "Группа", "Оценка"};

    @Override
    public int getRowCount() {
        return Repozitory.getInstance().countStudent();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnName[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Student student = Repozitory.getInstance().getStudent(rowIndex);
        String[] str = {String.valueOf(student.studentNumber + 1), student.lastName, student.firstName,
                student.patronymic, student.groopName, String.valueOf(student.mark)};
        return str[columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }
}

