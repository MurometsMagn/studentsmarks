package mainPackage.StudentGUI;

import com.sun.glass.ui.CommonDialogs;
import jdk.nashorn.internal.scripts.JO;
import mainPackage.commands.Repozitory;
import mainPackage.commands.Student;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showConfirmDialog;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 * Created by Smolentsev Il'ya on 28.12.2019.
 * <p>
 * Таблицу можно заполнять методом:
 * setValueAt(Object data, int row, int column);
 * <p>
 * Заголовки столбцов автоматически появляются на экране,
 * только если таблица заключена в панель JScrollPane
 */
public class MainMenuPage extends JPanel {
    JLabel label = new JLabel("Главное меню", SwingConstants.CENTER);

    static JTable jTable = new JTable(new MyTableModel()); //реализовано
    //DefaultTableModel tableModel = (DefaultTableModel) jTable.getModel(); // Default позволяет удалять строку
    JScrollPane jScrollPane = new JScrollPane(jTable);

    JButton buttonAdd = new JButton("Add");
    JButton buttonEdit = new JButton("Edit");
    JButton buttonDelete = new JButton("Delete");

    JLabel selectedLabel = new JLabel("Selected:");
    JLabel currentSelectionLabel = new JLabel("");
    int selectedRow = 0;
    JDialog deleteDialog = new JDialog();

    public MainMenuPage() {
        //Container contentPane = jPanel.getContentPane()
        // Container contentPane = jPanel.

        SpringLayout springLayout = new SpringLayout();
        setLayout(springLayout);

        //springLayout.putConstraint(SpringLayout.EAST, tableHeader, 10, SpringLayout.EAST, jTable);
        //Qt


        Spring interval = Spring.constant(10); //величина промежутка между компонентами
        Spring border = Spring.constant(10); //величина отступа от границ контейнера
        Spring positionX = border; //Текущее положение левого верхнего угла
        Spring positionY = border;
        Spring size = Spring.constant(70); //размер (кнопки)
        Spring maxSize = Spring.constant(0);//размер кнопки определяется по наибольшей

        add(label);
        springLayout.putConstraint(SpringLayout.NORTH, label, interval, SpringLayout.NORTH, this);
        springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, jScrollPane);

        add(jScrollPane);
        //размер компонента:
        SpringLayout.Constraints constraintsJTab = springLayout.getConstraints(jScrollPane);//new SpringLayout.Constraints();
        springLayout.putConstraint(SpringLayout.NORTH, jScrollPane, interval, SpringLayout.SOUTH, label);
        springLayout.putConstraint(SpringLayout.EAST, jScrollPane, Spring.minus(interval), SpringLayout.WEST, buttonAdd);
        springLayout.putConstraint(SpringLayout.WEST, jScrollPane, interval, SpringLayout.WEST, this);
        // springLayout.putConstraint(SpringLayout.SOUTH, jScrollPane, interval, SpringLayout.SOUTH, this);

        add(buttonAdd);
        SpringLayout.Constraints constraintsBtnAdd = springLayout.getConstraints(buttonAdd);
        springLayout.putConstraint(SpringLayout.NORTH, buttonAdd, 0, SpringLayout.NORTH, jScrollPane);
        springLayout.putConstraint(SpringLayout.EAST, buttonAdd, Spring.minus(interval), SpringLayout.EAST, this);

        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //AddButtonAct();
                new StartFrame(new AddStudentPage());
            }
        });

        add(buttonEdit);
        SpringLayout.Constraints constraintsBtnEdit = springLayout.getConstraints(buttonEdit);
        springLayout.putConstraint(SpringLayout.NORTH, buttonEdit, interval, SpringLayout.SOUTH, buttonAdd);
        springLayout.putConstraint(SpringLayout.EAST, buttonEdit, Spring.minus(interval), SpringLayout.EAST, this);

        buttonEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //ChangeButtonAct(Repozitory.getInstance().getStudent(selectedRow));
                JFrame f = new ChangeStudentPage(Repozitory.getInstance().getStudent(selectedRow), selectedRow);
            }
        });


        add(buttonDelete);
        SpringLayout.Constraints constraintsBtnDel = springLayout.getConstraints(buttonDelete);
        springLayout.putConstraint(SpringLayout.NORTH, buttonDelete, interval, SpringLayout.SOUTH, buttonEdit);
        springLayout.putConstraint(SpringLayout.EAST, buttonDelete, Spring.minus(interval), SpringLayout.EAST, this);

        final MainMenuPage self = this; //т.к. this невозм в анонимном классе
        buttonDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int result = JOptionPane.showConfirmDialog(self,
                        "delete " + Repozitory.getInstance().getStudent(selectedRow).lastName + "?",
                        "Confirm deleting", JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    Repozitory.getInstance().deleteStudent(selectedRow);
                    Repozitory.getInstance().writingFileBytes();
                    jTable.repaint();
                    currentSelectionLabel.repaint(); //не работает
                }
            }
        });

        //установка ширины кнопок
        maxSize = Spring.max(constraintsBtnAdd.getWidth(), constraintsBtnEdit.getWidth());
        maxSize = Spring.max(maxSize, constraintsBtnDel.getWidth());
        constraintsBtnAdd.setWidth(maxSize);
        constraintsBtnEdit.setWidth(maxSize);
        constraintsBtnDel.setWidth(maxSize);

        add(selectedLabel);
        springLayout.putConstraint(SpringLayout.NORTH, selectedLabel, interval, SpringLayout.SOUTH, jScrollPane);
        springLayout.putConstraint(SpringLayout.WEST, selectedLabel, 0, SpringLayout.WEST, jScrollPane);

        add(currentSelectionLabel);
        springLayout.putConstraint(SpringLayout.NORTH, currentSelectionLabel, 0, SpringLayout.NORTH, selectedLabel);
        springLayout.putConstraint(SpringLayout.WEST, currentSelectionLabel, interval, SpringLayout.EAST, selectedLabel);

        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //выделение ТОЛЬКО одной строки таблицы
        ListSelectionModel selModel = jTable.getSelectionModel();

        selModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                selectedRow = jTable.getSelectedRow();
                String value = jTable.getValueAt(selectedRow, 1).toString();
                currentSelectionLabel.setText(value);

                /*String result = "";
                int[] selectedRows = table.getSelectedRows();
                for(int i = 0; i < selectedRows.length; i++) {
                    int selIndex = selectedRows[i];
                    TableModel model = table.getModel(); //?
                    Object value = model.getValueAt(selIndex, 0);
                    result = result + value;
                    if(i != selectedRows.length - 1) {
                        result += ", ";
                    }
                }
                currentSelectionLabel.setText(result);*/
            }
        });

        //установка размеров контейнера (панели)
        /*SpringLayout.Constraints constraintsSL = springLayout.getConstraints(this);
        Spring tmp = Spring.sum(constraintsBtnAdd.getY(), constraintsBtnAdd.getWidth());
        tmp = Spring.sum(tmp, interval);
        constraintsSL.setConstraint(SpringLayout.EAST, tmp);
        */

    }
}

