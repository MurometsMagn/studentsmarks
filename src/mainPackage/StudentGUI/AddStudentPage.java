package mainPackage.StudentGUI;

import jdk.nashorn.internal.runtime.regexp.joni.SearchAlgorithm;
import mainPackage.commands.Repozitory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Smolentsev Il'ya on 02.01.2020.
 */
public class AddStudentPage extends JPanel {
    int strLength = Repozitory.getInstance().strLength;
    JLabel label = new JLabel("Добавление студента", SwingConstants.CENTER);

    JTextField fieldLastName = new JTextField("Введите фамилию", strLength);
    JTextField fieldFirstName = new JTextField("Введите имя", strLength);
    JTextField fieldPatronymic = new JTextField("Введите отчество", strLength);
    JTextField fieldGroop = new JTextField("Название группы", strLength);
    JTextField fieldMark = new JTextField("Введите оценку", strLength);
    JButton buttonOK = new JButton("OK");
    JButton buttonCancel = new JButton("Cancel");

    public AddStudentPage() {
  /*      //порядок не менять:
        super("Submenu");
        setSize(400, 400);
        //setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
*/
        SpringLayout springLayout = new SpringLayout();
        setLayout(springLayout);

        Spring interval = Spring.constant(10); //величина промежутка между компонентами
        Spring border = Spring.constant(10); //величина отступа от границ контейнера
        Spring positionX = border; //Текущее положение левого верхнего угла
        Spring positionY = border;
        Spring length = Spring.constant(strLength);

        add(label);
        //SpringLayout.Constraints constraintsLabel = springLayout.getConstraints(label);
        springLayout.putConstraint(SpringLayout.NORTH, label, interval, SpringLayout.NORTH, this);
        springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, fieldLastName);

        add(fieldLastName);
        //SpringLayout.Constraints constraintsFieldLastName = springLayout.getConstraints(fieldLastName);
        springLayout.putConstraint(SpringLayout.NORTH, fieldLastName, interval, SpringLayout.SOUTH, label);
        springLayout.putConstraint(SpringLayout.EAST, fieldLastName, Spring.minus(interval), SpringLayout.EAST, this);
        fieldLastName.setHorizontalAlignment(SwingConstants.CENTER);

        add(fieldFirstName);
       // SpringLayout.Constraints constraintsFieldFirstName = springLayout.getConstraints(fieldFirstName);
        springLayout.putConstraint(SpringLayout.NORTH, fieldFirstName, interval, SpringLayout.SOUTH, fieldLastName);
        springLayout.putConstraint(SpringLayout.EAST, fieldFirstName, Spring.minus(interval), SpringLayout.EAST, this);
        fieldFirstName.setHorizontalAlignment(SwingConstants.CENTER);

        add(fieldPatronymic);
        //SpringLayout.Constraints constraintsFieldPatronymic = springLayout.getConstraints(fieldPatronymic);
        springLayout.putConstraint(SpringLayout.NORTH, fieldPatronymic, interval, SpringLayout.SOUTH, fieldFirstName);
        springLayout.putConstraint(SpringLayout.EAST, fieldPatronymic, Spring.minus(interval), SpringLayout.EAST, this);
        fieldPatronymic.setHorizontalAlignment(SwingConstants.CENTER);

        add(fieldGroop);
       // SpringLayout.Constraints constraintsFieldGroop = springLayout.getConstraints(fieldGroop);
        springLayout.putConstraint(SpringLayout.NORTH, fieldGroop, interval, SpringLayout.SOUTH, fieldPatronymic);
        springLayout.putConstraint(SpringLayout.EAST, fieldGroop, Spring.minus(interval), SpringLayout.EAST, this);
        fieldGroop.setHorizontalAlignment(SwingConstants.CENTER);

        add(fieldMark);
        //SpringLayout.Constraints constraintsFieldMark = springLayout.getConstraints(fieldMark);
        springLayout.putConstraint(SpringLayout.NORTH, fieldMark, interval, SpringLayout.SOUTH, fieldGroop);
        springLayout.putConstraint(SpringLayout.EAST, fieldMark, Spring.minus(interval), SpringLayout.EAST, this);
        fieldMark.setHorizontalAlignment(SwingConstants.CENTER);

        add(buttonOK);
        //SpringLayout.Constraints constraintsBtnOK = springLayout.getConstraints(buttonOK);
        springLayout.putConstraint(SpringLayout.NORTH, buttonOK, interval, SpringLayout.SOUTH, fieldMark);
        springLayout.putConstraint(SpringLayout.EAST, buttonOK, Spring.minus(interval), SpringLayout.WEST, buttonCancel);


        add(buttonCancel);


        //SpringLayout.Constraints constraintsBtnCancel = springLayout.getConstraints(buttonCancel);
        springLayout.putConstraint(SpringLayout.NORTH, buttonCancel, 0, SpringLayout.NORTH, buttonOK);
        springLayout.putConstraint(SpringLayout.EAST, buttonCancel, 0, SpringLayout.EAST, fieldMark);
    }

    void activateComponent(JComponent comp) {
        // comp.setHorizontalAlignment(SwingConstants.CENTER); //не работает
    }
}
