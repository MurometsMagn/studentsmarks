package mainPackage.StudentGUI;

import mainPackage.commands.Repozitory;
import mainPackage.commands.Student;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Smolentsev Il'ya on 28.12.2019.
 */
public class StartFrame extends JFrame {

    public StartFrame() {
        this(new MainMenuPage());
    }

    public StartFrame(JPanel jPanel) {
        //порядок не менять:
        super("Students");
        setSize(800, 600);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);

        //add(jPanel, BorderLayout.CENTER);
        add(jPanel);
    }
}
