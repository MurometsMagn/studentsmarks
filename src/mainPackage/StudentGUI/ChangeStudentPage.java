package mainPackage.StudentGUI;

import mainPackage.commands.Repozitory;
import mainPackage.commands.Student;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Smolentsev Il'ya on 02.01.2020.
 */
public class ChangeStudentPage extends JFrame {
    //JFrame frame = new JFrame();

    int strLength = Repozitory.getInstance().strLength;
    JLabel label = new JLabel("Изменение данных студента", SwingConstants.CENTER);

    JTextField fieldLastName = new JTextField(strLength);
    JTextField fieldFirstName = new JTextField(strLength);
    JTextField fieldPatronymic = new JTextField(strLength);
    JTextField fieldGroop = new JTextField(strLength);
    JTextField fieldMark = new JTextField(strLength);

    //JList<Student> jList = new JList<>();

    //JButton buttonPlus = new JButton("+"); //?
    // JButton buttonCrest = new JButton("X"); //?
    JButton buttonOK = new JButton("OK");
    JButton buttonCancel = new JButton("Cancel");

    public ChangeStudentPage() { //?
        this(new Student(), 1);
    }

    public ChangeStudentPage(Student student, int selectedRow) { //конструктор принимает студента
        //порядок не менять:
        super("Submenu");
        setSize(400, 400);
        //setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);

      //  frame = this; //надо?

        //JPanel jPanel = new JPanel();
        //add(jPanel);
        SpringLayout springLayout = new SpringLayout();
        setLayout(springLayout);

        Spring interval = Spring.constant(10); //величина промежутка между компонентами
        Spring border = Spring.constant(10); //величина отступа от границ контейнера

        add(label);
        springLayout.putConstraint(SpringLayout.NORTH, label, interval, SpringLayout.NORTH, this);
        springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, fieldLastName);

        add(fieldLastName);
        springLayout.putConstraint(SpringLayout.NORTH, fieldLastName, interval, SpringLayout.SOUTH, label);
        //springLayout.putConstraint(SpringLayout.EAST, fieldLastName, Spring.minus(interval), SpringLayout.EAST, this);
        springLayout.putConstraint(SpringLayout.WEST, fieldLastName, interval, SpringLayout.WEST, this);
        fieldLastName.setHorizontalAlignment(SwingConstants.CENTER);
        fieldLastName.setText(student.lastName);

        add(fieldFirstName);
        springLayout.putConstraint(SpringLayout.NORTH, fieldFirstName, interval, SpringLayout.SOUTH, fieldLastName);
        springLayout.putConstraint(SpringLayout.EAST, fieldFirstName, 0, SpringLayout.EAST, fieldLastName);
        fieldFirstName.setHorizontalAlignment(SwingConstants.CENTER);
        fieldFirstName.setText(student.firstName);

        add(fieldPatronymic);
        springLayout.putConstraint(SpringLayout.NORTH, fieldPatronymic, interval, SpringLayout.SOUTH, fieldFirstName);
        springLayout.putConstraint(SpringLayout.EAST, fieldPatronymic, 0, SpringLayout.EAST, fieldLastName);
        fieldPatronymic.setHorizontalAlignment(SwingConstants.CENTER);
        fieldPatronymic.setText(student.patronymic);

        add(fieldGroop);
        springLayout.putConstraint(SpringLayout.NORTH, fieldGroop, interval, SpringLayout.SOUTH, fieldPatronymic);
        springLayout.putConstraint(SpringLayout.EAST, fieldGroop, 0, SpringLayout.EAST, fieldLastName);
        fieldGroop.setHorizontalAlignment(SwingConstants.CENTER);
        fieldGroop.setText(student.groopName);

        add(fieldMark);
        springLayout.putConstraint(SpringLayout.NORTH, fieldMark, interval, SpringLayout.SOUTH, fieldGroop);
        springLayout.putConstraint(SpringLayout.EAST, fieldMark, 0, SpringLayout.EAST, fieldLastName);
        fieldMark.setHorizontalAlignment(SwingConstants.CENTER);
        fieldMark.setText(String.valueOf(student.mark));

        add(buttonOK);
        springLayout.putConstraint(SpringLayout.NORTH, buttonOK, interval, SpringLayout.SOUTH, fieldMark);
        springLayout.putConstraint(SpringLayout.EAST, buttonOK, Spring.minus(interval), SpringLayout.WEST, buttonCancel);

        final ChangeStudentPage self = this;
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //1 удалить студента
                //2 добавить в репоз измененного ст-та
                //3 сохранить
                //4 отобразить в таблице
                //5 закрыть окно change

                //1
                Repozitory.getInstance().deleteStudent(selectedRow);
                //2
                Student student1 = new Student(selectedRow, fieldLastName.getText(), fieldFirstName.getText(),
                        fieldPatronymic.getText(), fieldGroop.getText(), Integer.parseInt(fieldMark.getText()));
                Repozitory.getInstance().addStudent(student1);
                //3
                Repozitory.getInstance().writingFileBytes();
                //4 не работает

                MainMenuPage.jTable.repaint();
                //5
                self.dispose();
            }
        });

        add(buttonCancel);
        springLayout.putConstraint(SpringLayout.NORTH, buttonCancel, 0, SpringLayout.NORTH, buttonOK);
        springLayout.putConstraint(SpringLayout.EAST, buttonCancel, 0, SpringLayout.EAST, fieldMark);

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //CancelButtonAct();
                dispose();
            }
        });
    }

    private void CancelButtonAct() {
        this.dispose();
        // this.dispose;  //ошибка: нужен фрейм. Перенести в конструктор StartFrame?
    }
}
